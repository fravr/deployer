#!/usr/bin/env python

import sys
import json

RAW = ''

for line in sys.stdin:
    RAW += line

data = json.loads(RAW)

result = data
for i in range(1, len(sys.argv)):
    key = sys.argv[i]
    result = result[key]

print result
