#!/usr/bin/env bash

Script_Dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
Root_Dir=/srv
Base_Name=base

setupEnv () {
  # if [[ -z $Hostname ]]; then
  #   if [[ -z $No_Hostname ]]; then
  #     Hostname=\
  #       $(curl -s --connect-timeout 3 http://169.254.169.254/metadata/v1/hostname)

  #     if [[ -z $Hostname ]]; then
  #       No_Hostname=true
  #       echo 'export No_Hostname=true'> /etc/profile.d/hostname.sh
  #     else
  #       echo 'export Hostname='$Hostname > /etc/profile.d/hostname.sh
  #     fi
  #   fi
  # fi

  # [[ -z $Service_Name ]] && [[ ! -z $Hostname ]] && \
  #   Service_Name=$(echo $Hostname | sed 's/-/ /' | awk '{ print $1 }')
  # [[ -z $Service_Name ]] && echo "Service_Name not specified!" && exit 1

  # [[ -z $Service_Branch ]] && Service_Branch=master

  [[ -z $Base_Branch ]] && Base_Branch=master

  [[ -z $VAULT_ADDR ]] && VAULT_ADDR=https://172.31.31.1:8200
}

vaultUserId () {
  # Attempt to get Digital Ocean UID
  local ID=$(curl -s --connect-timeout 3 http://169.254.169.254/metadata/v1/id)
  [[ $? == 0 ]] && [[ ! -z $ID ]] && echo $ID && return 0

  # Fallback to MAC as id
  local HOST=$(curl -v --connect-timeout 3 $VAULT_ADDR 2>&1 | grep "host .* left" | sed 's/.* host \(.*\) left .*/\1/')
  [[ -z $HOST ]] && echo Cannot get host name && exit 1

  local IP=$(dig $HOST +short | head -n 1)
  [[ -z $IP ]] && echo Cannot get ip address && exit 1

  local INTERFACE=$(ip -o route get $IP | awk '{print $3}')
  local ID=$(cat /sys/class/net/$INTERFACE/address)
  [[ ! -z $ID ]] && echo $ID && return 0

  # Otherwise return error code
  return 1
}

vaultLogin () {
  # Get vault credentials
  [[ -z $Service_ID ]] && echo 'Missing Service_ID' && return 1
  local User_ID=$(vaultUserId)
  [[ $? != 0 ]] && return 1
  local User_ID=$(echo -n $User_ID | shasum -a 256 | awk '{print $1}')
  [[ -z $User_ID ]] && return 1

  # Get vault token
  Token_Data=$(curl -s $VAULT_ADDR/v1/auth/app-id/login -d '{ "app_id": "'$Service_ID'", "user_id": "'$User_ID'"}')
  [[ $? != 0 ]] && return 1
  echo $Token_Data | $Script_Dir/parseJson.py auth client_token
}


installPackages () {
  if command -v git >/dev/null 2>&1; then
    return 0
  fi
  add-apt-repository ppa:git-core/ppa -y # Latest git stable repo
  [[ $? != 0 ]] && exit 1
  apt-get update
  [[ $? != 0 ]] && exit 1
  apt-get install -y git # Need git to pull repos
  [[ $? != 0 ]] && exit 1
}

pullBranch () {
  echo "Checking for latest commit of branch $2 from $1"
  local SHA=$(git ls-remote git@bitbucket.org:fravr/${1}.git $2 | awk '{ print $1 }')
  [[ -z $SHA ]] && return 1
  pullSHA $1 $2 $SHA $3
}

pullSHA () {
  local Repo=$1
  local Branch=$2
  local SHA=$3
  local Dir_Name=$4
  [[ -z $Dir_Name ]] && Dir_Name=$Repo
  local Path=$Root_Dir/$Dir_Name

  if [[ ! -e $Path/$SHA ]]; then
    echo "Pulling SHA $SHA in branch $Branch from $Repo"
    mkdir -p $Path
    git clone -b $Branch git@bitbucket.org:fravr/$Repo.git $Path/$SHA
    [[ $? != 0 ]] && return 1
    (cd $Path/$SHA && git reset --hard $SHA)
  fi

  # Ensure directory date is latest
  touch $Path/$SHA

  # Atomic change of symlink
  (cd $Path && ln -s $Path/$SHA tmplink && mv -Tf tmplink next)
  return 0
}

# Checks consul for relese info and set git branch and SHA
checkRelease () {
  return 0
}

setupEnv
[[ $Is_Virtual != 1 ]] && checkRelease

# Install salt
if [[ ! -e /etc/salt ]]; then
  $Script_Dir/install_salt.sh -G stable 2015.8
  [[ $? != 0 ]] && exit 1
fi

# Install packages
installPackages

# Write salt config
cat $Script_Dir/salt/minion.sls > /etc/salt/minion

# Install SSH config
salt-call state.sls --retcode-passthrough --local --config-dir=$Script_Dir --file-root=$Script_Dir ssh
[[ $? != 0 ]] && exit 1

rm -rf /var/cache/salt
salt-call saltutil.sync_grains --retcode-passthrough
[[ $? != 0 ]] && exit 1

# Install Git repo keys
if [[ $Is_Virtual != 1 ]]; then
  # Check if Vault is up and ready
  curl -s --retry 999 --retry-max-time 60 $VAULT_ADDR/v1/sys/health >/dev/null
  [[ $? != 0 ]] && exit 1

  # Login
  Token=$(vaultLogin)
  [[ $? != 0 ]] && exit 1

  # TODO: Get git repo key
  exit 1
fi

# Pull repos
if [[ -z $Base_SHA ]]; then
  pullBranch $Base_Name $Base_Branch
  [[ $? != 0 ]] && exit 1
else
  pullSHA $Base_Name $Base_Branch $Base_SHA
  [[ $? != 0 ]] && exit 1
fi
# if [[ ! -e $Root_Dir/service ]]; then
#   salt-call saltutil.sync_grains --retcode-passthrough
#   [[ $? != 0 ]] && exit 1
# fi

# if [[ -z $Service_SHA ]]; then
#   pullBranch $Service_Name $Service_Branch service
#   [[ $? != 0 ]] && exit 1
# else
#   pullSHA $Service_Name $Service_Branch $Service_SHA service
#   [[ $? != 0 ]] && exit 1
# fi

salt-call saltutil.sync_grains --retcode-passthrough
[[ $? != 0 ]] && exit 1
salt-call state.highstate --retcode-passthrough
[[ $? != 0 ]] && exit 1

(cd $Root_Dir/base && cp -rf --preserve=links next current)
# (cd $Root_Dir/service && cp -rf --preserve=links next current)

exit 0
