#!/usr/bin/env bash

# Save Script_Dir variable and restore it later
Script_Dir_Old=$Script_Dir
Script_Dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

Is_Virtual="$([[ -d /vagrant ]] && echo 1 || 0)"
Root_Dir=/srv
Base_Name=base

setupEnv () {
  local P=$Root_Dir/config
  [[ -e $P ]] && source $P

  [[ -z $Service_Name ]] && echo "Service_Name not specified!" && exit 1
  [[ -z $Service_Branch ]] && export Service_Branch=master

  [[ -z $Base_Branch ]] && export Base_Branch=master

  [[ -z $VAULT_ADDR ]] && export VAULT_ADDR=https://172.31.31.31:8200
}

pullBranch () {
  local Dir_Name=$3
  [[ -z $3 ]] && Dir_Name=$1

  local P=$Root_Dir/$Dir_Name/$2
  mkdir -p $P
  rm -rf $P

  git clone -b $2 --depth=1 git@bitbucket.org:fravr/$1.git $P
  # (cd $Root_Dir/$1 && ln -s $P current_tmp)
  (cd $Root_Dir/$Dir_Name && ln -s $P tmplink && mv -Tf tmplink next)
}

pullSHA () {
  local N=$3
  [[ -z $3 ]] && N=$1

  local P=$Root_Dir/$Dir_Name/$2
  mkdir -p $P
  rm -rf $P

  git clone -b $2 git@bitbucket.org:fravr/$1.git $P
  (cd $P && git reset --hard $2)
  (cd $Root_Dir/$Dir_Name && ln -s $P tmplink && mv -Tf tmplink next)
}

vaultUserId () {
  # Attempt to get Digital Ocean UID
  local ID=$(curl --connect-timeout 3 http://169.254.169.254/metadata/v1/id)
  [[ $? == 0 ]] && [[ ! -z $ID ]] && echo $ID && return 0

  # Fallback to MAC as id
  local HOST=$(curl -v --connect-timeout 3 $VAULT_ADDR 2>&1 | grep "host .* left" | sed 's/.* host \(.*\) left .*/\1/')
  [[ -z $HOST ]] && echo Cannot get host name && exit 1

  local IP=$(dig $HOST +short | head -n 1)
  [[ -z $IP ]] && echo Cannot get ip address && exit 1

  local INTERFACE=$(ip -o route get $IP | awk '{print $3}')
  local ID=$(cat /sys/class/net/$INTERFACE/address)
  [[ ! -z $ID ]] && echo $ID && return 0

  # Otherwise return error code
  return 1
}

vaultLogin () {
  # Get vault credentials
  [[ -z $Service_ID ]] && echo 'Missing Service_ID' && return 1
  local User_ID=$(vaultUserId)
  [[ $? != 0 ]] && return 1
  local User_ID=$(echo -n $User_ID | shasum -a 256 | awk '{print $1}')
  [[ -z $User_ID ]] && return 1

  # Get vault token
  Token_Data=$(curl -s $VAULT_ADDR/v1/auth/app-id/login -d '{ "app_id": "'$Service_ID'", "user_id": "'$User_ID'"}')
  [[ $? != 0 ]] && return 1
  echo $Token_Data | $Script_Dir/parseJson.py auth client_token
}

setupEnv

Script_Dir=$Script_Dir_Old
