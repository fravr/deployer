# For development purposes, always fail if any state fails. This makes it much
# easier to ensure first-runs will succeed.
failhard: True

# Show terse output for successful states and full output for failures.
state_output: mixed

# Only show changes
state_verbose: False

# Show basic information about what Salt is doing during its highstate. Set
# this to critical to disable logging output.
log_level: info

# Never try to connect to a master.
file_client: local
local: True

# Path to the states, files and templates.
file_roots:
  base:
    - /srv/service/next/salt/config/states
    - /srv/base/next/salt/config/states

# Path to pillar variables.
pillar_roots:
  base:
    - /srv/service/next/salt/config/pillar
    - /srv/base/next/salt/config/pillar

# module_dirs:
#   base:
#     - /srv/service/next/salt/config/modules
#     - /srv/base/next/salt/config/modules

# # Path to custom grain modules
# grains_dirs:
#   - /srv/base/current/grains

# ext_pillar:
#  -
